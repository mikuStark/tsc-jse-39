package ru.tsc.karbainova.tm.dto;

import lombok.*;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.entity.IWBS;
import ru.tsc.karbainova.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "tm_task")
public class Task extends AbstractOwnerEntity implements IWBS {

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }


    @Column
    @NonNull
    private String name;
    @Column
    @Nullable
    private String description;
    @NonNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;
    @Nullable
    @Column(name = "project_id")
    private String projectId = null;
    @Nullable
    @Column(name = "start_date")
    private Date startDate;
    @Nullable
    @Column(name = "finish_date")
    private Date finishDate;
    @NonNull
    @Column
    private Date created = new Date();

    @Override
    public String toString() {
        return " " + name + " ";
    }

}
