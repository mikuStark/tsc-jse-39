package ru.tsc.karbainova.tm.api.service;

public interface ServiceLocator {
    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

    IUserService getUserService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

    IAdminUserService getAdminUserService();
}
