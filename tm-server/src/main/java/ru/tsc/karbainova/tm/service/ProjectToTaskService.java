package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;

public class ProjectToTaskService {
    //implements IProjectToTaskService
    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectToTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

//    @Override
//    public List<Task> findTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
//        if (projectId.isEmpty()) throw new EmptyIdException();
//        return taskRepository.findAllTaskByProjectId(userId, projectId);
//    }
//
//    @Override
//    public Task taskBindById(@NonNull String userId, @NonNull String projectId, @NonNull String taskId) {
//        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
//        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
//        if (!projectRepository.existsById(userId, projectId)) throw new EmptyIdException();
//        if (!taskRepository.existsById(userId, taskId)) throw new EmptyIdException();
//        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
//    }
//
//    @Override
//    public Task taskUnbindById(@NonNull String userId, @NonNull String projectId, @NonNull String taskId) {
//        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
//        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
//        if (!projectRepository.existsById(userId, projectId)) throw new EmptyIdException();
//        if (!taskRepository.existsById(userId, taskId)) throw new EmptyIdException();
//        return taskRepository.taskUnbindById(userId, taskId);
//    }
//
//    @Override
//    public void removeAllTaskByProjectId(@NonNull String userId, @NonNull String projectId) {
//        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
//        taskRepository.removeAllTaskByProjectId(userId, projectId);
//    }
//
//    @Override
//    public void removeById(@NonNull String userId, @NonNull String projectId) {
//        removeAllTaskByProjectId(userId, projectId);
//        projectRepository.removeById(userId, projectId);
//    }
}
