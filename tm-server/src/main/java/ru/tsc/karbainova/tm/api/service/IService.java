package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IRepository;

import ru.tsc.karbainova.tm.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

//    void addAll(Collection<E> entities);
//
//    void clear();
//
//    void remove(E entity);
}
